# Public Scripts

This is a public repository where I share useful little scripts for a wide variety of tasks and written in a wide variety of languages.

## Getting started

You may download it by cloning the repo:

```
git clone https://gitlab.com/diego.nuevo/public-scripts.git
```

## License

All code in this repository is licensed under the GNU General Public License v3.0